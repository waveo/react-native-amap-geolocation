package cn.qiuxiang.react.geolocation;

import android.location.Geocoder;
import android.util.Log;

import androidx.annotation.NonNull;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.geocoder.GeocodeAddress;
import com.amap.api.services.geocoder.GeocodeQuery;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeAddress;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.List;

@SuppressWarnings("unused")
public class AMapGeolocationModule extends ReactContextBaseJavaModule implements AMapLocationListener, GeocodeSearch.OnGeocodeSearchListener {

    private static final String TAG = AMapGeolocationModule.class.getSimpleName();

    private ReactApplicationContext reactContext;
    private DeviceEventManagerModule.RCTDeviceEventEmitter eventEmitter;
    private AMapLocationClient client;
    private GeocodeSearch searcher;
    private AMapLocationClientOption option = new AMapLocationClientOption();

    AMapGeolocationModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @NonNull
    @Override
    public String getName() {
        return "AMapGeolocation";
    }

    @Override
    public void onLocationChanged(AMapLocation location) {
        if (location != null) {
            eventEmitter.emit("AMapGeolocation", toJSON(location));
        }
    }

    @ReactMethod
    public void init(String key, Promise promise) {
        if (client != null) {
            client.onDestroy();
        }

        AMapLocationClient.setApiKey(key);
        client = new AMapLocationClient(reactContext);
        client.setLocationListener(this);
        searcher = new GeocodeSearch(reactContext);
        searcher.setOnGeocodeSearchListener(this);
        eventEmitter = reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class);
        promise.resolve(null);
    }

    @ReactMethod
    public void start() {
        client.startLocation();
    }

    @ReactMethod
    public void stop() {
        client.stopLocation();
    }

    @ReactMethod
    public void geocoder(ReadableMap options, Promise promise) {
        Log.d(TAG, "invoke search options ==> " + options.toString());
        try {
            RegeocodeQuery query = new RegeocodeQuery(new LatLonPoint(options.getDouble("lat"), options.getDouble("lon")), 1000, GeocodeSearch.AMAP);
            RegeocodeAddress address = searcher.getFromLocation(query);
            promise.resolve(geoToJSON(address, query));
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    /**
     * 根据地理名称关键字搜索地址
     * @param options { "location": "", "city": ""} // city: 查询城市名称、城市编码或行政区划代码。location : 查询的地理名称。
     * @param promise
     */
    @ReactMethod
    public void reverseGeocoder(ReadableMap options, Promise promise) {
        Log.d(TAG, "invoke search options ==> " + options.toString());
        try {
            GeocodeQuery query = new GeocodeQuery(options.getString("location"), options.getString("city"));
            List<GeocodeAddress> addresses = searcher.getFromLocationName(query);
            promise.resolve(addressToJSON(addresses, query));
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void isStarted(Promise promise) {
        promise.resolve(client.isStarted());
    }

    @ReactMethod
    public void getLastKnownLocation(Promise promise) {
        promise.resolve(toJSON(client.getLastKnownLocation()));
    }

    @ReactMethod
    public void setOnceLocation(boolean value) {
        option.setOnceLocation(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setWifiScan(boolean value) {
        option.setWifiScan(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setInterval(int interval) {
        option.setInterval(interval);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setSensorEnable(boolean value) {
        option.setSensorEnable(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setOpenAlwaysScanWifi(boolean value) {
        AMapLocationClientOption.setOpenAlwaysScanWifi(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setNeedAddress(boolean value) {
        option.setNeedAddress(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setOnceLocationLatest(boolean value) {
        option.setOnceLocationLatest(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setMockEnable(boolean value) {
        option.setMockEnable(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setLocationCacheEnable(boolean value) {
        option.setLocationCacheEnable(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setGpsFirst(boolean value) {
        option.setGpsFirst(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setHttpTimeout(int value) {
        option.setHttpTimeOut(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setGpsFirstTimeout(int value) {
        option.setGpsFirstTimeout(value);
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setLocationMode(String mode) {
        option.setLocationMode(AMapLocationClientOption.AMapLocationMode.valueOf(mode));
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setLocationPurpose(String purpose) {
        option.setLocationPurpose(AMapLocationClientOption.AMapLocationPurpose.valueOf(purpose));
        client.setLocationOption(option);
    }

    @ReactMethod
    public void setGeoLanguage(String language) {
        option.setGeoLanguage(AMapLocationClientOption.GeoLanguage.valueOf(language));
        client.setLocationOption(option);
    }

    private ReadableArray addressToJSON(List<GeocodeAddress> addresses, GeocodeQuery query) {
        if (addresses == null) {
            return null;
        }
        WritableArray results = Arguments.createArray();

        for (GeocodeAddress address : addresses)
        {
            WritableMap map = Arguments.createMap();
            map.putString("adcode", address.getAdcode());
            map.putString("country", "中国");
            map.putString("countryCode", "CN");
            map.putString("privince", address.getProvince());
            map.putString("city", address.getCity());
            map.putString("district", address.getDistrict());
            map.putString("neighborhood", address.getNeighborhood());
            map.putString("towership", address.getTownship());
            map.putString("buidling", address.getBuilding());
            map.putString("address", address.getFormatAddress());
            map.putDouble("latitude", address.getLatLonPoint().getLatitude());
            map.putDouble("longitude", address.getLatLonPoint().getLongitude());
            map.putString("level", address.getLevel());

            results.pushMap(map);
        }
        return results;
    }

    private ReadableMap geoToJSON(RegeocodeAddress address, RegeocodeQuery query) {
        if (address == null) {
            return null;
        }
        WritableMap map = Arguments.createMap();
        map.putString("formattedAddress", address.getFormatAddress());
        map.putString("country", address.getCountry());
        map.putString("countryCode", "CN"); // XXX
        map.putString("adminArea", address.getProvince());
        map.putString("locality", address.getCity());
        map.putString("cityCode", address.getCityCode());
        map.putString("subLocality", address.getDistrict());
        map.putString("building", address.getBuilding());
        map.putString("neighborhood", address.getNeighborhood());
        map.putString("towncode", address.getTowncode());
        map.putString("township", address.getTownship());
        map.putString("street", address.getStreetNumber().getStreet());
        map.putString("streetNumber", address.getStreetNumber().getNumber());
        map.putString("adCode", address.getAdCode());


        WritableMap position = Arguments.createMap();
        position.putDouble("lng", query.getPoint().getLongitude());
        position.putDouble("lat", query.getPoint().getLatitude());
        map.putMap("position", position);
        return map;
    }

    private ReadableMap toJSON(AMapLocation location) {
        if (location == null) {
            return null;
        }
        WritableMap map = Arguments.createMap();
        map.putInt("errorCode", location.getErrorCode());
        map.putString("errorInfo", location.getErrorInfo());
        map.putString("locationDetail", location.getLocationDetail());
        if (location.getErrorCode() == AMapLocation.LOCATION_SUCCESS) {
            map.putDouble("timestamp", location.getTime());
            map.putDouble("accuracy", location.getAccuracy());
            map.putDouble("latitude", location.getLatitude());
            map.putDouble("longitude", location.getLongitude());
            map.putDouble("altitude", location.getAltitude());
            map.putDouble("speed", location.getSpeed());
            map.putDouble("heading", location.getBearing());
            map.putInt("locationType", location.getLocationType());
            map.putString("coordinateType", location.getCoordType());
            map.putInt("gpsAccuracy", location.getGpsAccuracyStatus());
            map.putInt("trustedLevel", location.getTrustedLevel());
            if (!location.getAddress().isEmpty()) {
                map.putString("address", location.getAddress());
                map.putString("description", location.getDescription());
                map.putString("poiName", location.getPoiName());
                map.putString("country", location.getCountry());
                map.putString("province", location.getProvince());
                map.putString("city", location.getCity());
                map.putString("cityCode", location.getCityCode());
                map.putString("district", location.getDistrict());
                map.putString("street", location.getStreet());
                map.putString("streetNumber", location.getStreetNum());
                map.putString("adCode", location.getAdCode());
            }
        }
        return map;
    }

    @Override
    public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {

    }

    @Override
    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

    }
}
